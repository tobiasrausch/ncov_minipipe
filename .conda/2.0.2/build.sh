#!/bin/bash

 
cp_if_file_exists(){
	if [ -f "$1" ] || [ -d "$1" ]; then
		cp -fr "$1" ${2-""}  
	fi
}

OPTPATH="${PREFIX}/opt/ncov_minipipe"
mkdir -p "${OPTPATH}" 
cp -fr {tests,envs,scripts,ncov_minipipe.py,__version__.py,ncov_minipipe.Rmd,ncov_minipipe.snake,ncov_minipipe.config,seq_parser,rules} "${PREFIX}/opt/ncov_minipipe/."
cp_if_file_exists .git "${OPTPATH}"
cp_if_file_exists adapter.fasta "${OPTPATH}"
chmod +x "${OPTPATH}/ncov_minipipe.py"
chmod +x "${OPTPATH}/tests/quicktest.sh"
ln -frs  "${OPTPATH}/ncov_minipipe.py" \
         "${PREFIX}/bin/ncov_minipipe"
ln -frs  "${OPTPATH}/ncov_minipipe.py" \
         "${PREFIX}/bin/covpipe"
ln -frs  "${OPTPATH}/tests/quicktest.sh" \
         "${PREFIX}/bin/covpipe-quicktest"
